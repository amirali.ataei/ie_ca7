package com.example.CA6.exception;

public class HadPassedCourseException extends Exception{
    private String name;
    public HadPassedCourseException(String _name) {
        name = _name;
    }
    @Override
    public String toString() {
        return name + " had been passed.";
    }
}

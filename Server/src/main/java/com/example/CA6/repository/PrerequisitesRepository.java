package com.example.CA6.repository;

import com.example.CA6.service.Course;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PrerequisitesRepository extends Repository<String, String> {
    private static final String TABLE_NAME = "Prerequisites";
    private static PrerequisitesRepository instance;

    public static PrerequisitesRepository getInstance() {
        if (instance == null) {
            try {
                instance = new PrerequisitesRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in PrerequisitesRepository.create query.");
            }
        }
        return instance;
    }

    private PrerequisitesRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format("CREATE TABLE IF NOT EXISTS %s (\n" +
                        "  `cCode` char(225) NOT NULL,\n" +
                        "  `pCode` char(225) DEFAULT NULL,\n" +
                        "  KEY `pCode_idx` (`pCode`),\n" +
                        "  KEY `cCode_idx` (`cCode`),\n" +
                        "  CONSTRAINT `cCode` FOREIGN KEY (`cCode`) REFERENCES `Course` (`code`) ON DELETE CASCADE,\n" +
                        "  CONSTRAINT `pCode` FOREIGN KEY (`pCode`) REFERENCES `Course` (`code`)\n" +
                        ");", TABLE_NAME)
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }


    @Override
    protected String getFindByIdStatement() {
        return String.format("Select * From %s Where %s.cCode = ?", TABLE_NAME, TABLE_NAME);
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, String id) throws SQLException {
        st.setString(1, id);
    }

    @Override
    protected String getInsertStatement() {
        return String.format("INSERT INTO %s(cCode, pCode) Select ?, ? Where not exists(Select * from %s where cCode = ? AND pCode = ?);", TABLE_NAME, TABLE_NAME);
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, String data) throws SQLException {
    }

    @Override
    protected String getFindAllStatement() {
        return null;
    }

    @Override
    protected void fillFindBySearchTypeValues(PreparedStatement st, String search, String type) throws SQLException {

    }

    @Override
    protected String convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        return rs.getString(2);
    }

    @Override
    protected String getFindBySearchType() {
        return null;
    }

    @Override
    protected ArrayList<String> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        ArrayList<String> courses = new ArrayList<>();
        while (rs.next()) {
            courses.add(this.convertResultSetToDomainModel(rs));
        }
        return courses;
    }

    @Override
    protected void fillInsertByIdValues(PreparedStatement st, String cCode, String pCode) throws SQLException {
        st.setString(1, cCode);
        st.setString(2, pCode);
        st.setString(3, cCode);
        st.setString(4, pCode);
    }

    @Override
    protected String getFindBySearch() {
        return null;
    }

    @Override
    protected void fillFindBySearchValues(PreparedStatement st, String search) throws SQLException {

    }

    @Override
    protected void fillFindByTermValues(PreparedStatement st, String id) {

    }

    @Override
    protected String getFindByTermStatement() {
        return null;
    }

    @Override
    protected void fillFindByEmailValues(PreparedStatement st, String email) throws SQLException {

    }

    @Override
    protected String getFindByEmailStatement() {
        return null;
    }
}

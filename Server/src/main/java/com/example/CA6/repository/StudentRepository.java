package com.example.CA6.repository;

import com.example.CA6.service.Grade;
import com.example.CA6.service.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StudentRepository extends Repository<Student, String> {
    private static final String TABLE_NAME = "Student";
    private static StudentRepository instance;

    public static StudentRepository getInstance() {
        if (instance == null) {
            try {
                instance = new StudentRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in StudentRepository.create query.");
            }
        }
        return instance;
    }

    private StudentRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format("CREATE TABLE IF NOT EXISTS %s (\n" +
                        "  `id` char(225) NOT NULL,\n" +
                        "  `name` varchar(225) NOT NULL,\n" +
                        "  `email` varchar(225) NOT NULL,\n" +
                        "  `password` int NOT NULL,\n" +
                        "  `secondName` varchar(225) NOT NULL,\n" +
                        "  `birthDate` varchar(225) NOT NULL,\n" +
                        "  `field` varchar(225) NOT NULL,\n" +
                        "  `faculty` varchar(225) NOT NULL,\n" +
                        "  `level` varchar(225) NOT NULL,\n" +
                        "  `status` varchar(225) NOT NULL,\n" +
                        "  `img` varchar(225) NOT NULL,\n" +
                        "  PRIMARY KEY (`id`,`email`)\n" +
                        ");", TABLE_NAME)
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }


    @Override
    protected String getFindByIdStatement() {
        return String.format("SELECT " +
                "s.id, " +
                "s.name, " +
                "s.email, " +
                "s.password, " +
                "s.secondName, " +
                "s.birthDate, " +
                "s.field, " +
                "s.faculty, " +
                "s.level, " +
                "s.status, " +
                "s.img " +
                "FROM %s WHERE s.id = ?;", TABLE_NAME + " s");
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, String id) throws SQLException {
        st.setString(1, id);
    }

    @Override
    protected String getInsertStatement() {
        return String.format("INSERT INTO %s(" +
                "id, " +
                "name, " +
                "email, " +
                "password, " +
                "secondName, " +
                "birthDate, " +
                "field, " +
                "faculty, " +
                "level, " +
                "status, " +
                "img)" +
                "Select ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? Where not exists(Select * From %s where id = ? OR email = ?);", TABLE_NAME, TABLE_NAME);
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, Student data) throws SQLException {
        st.setString(1, data.getStudentId());
        st.setString(2, data.getName());
        st.setString(3, data.getEmail());
        st.setInt(4, data.getPassword().hashCode());
        st.setString(5, data.getSecondName());
        st.setString(6, data.getBirthDate());
        st.setString(7, data.getField());
        st.setString(8, data.getFaculty());
        st.setString(9, data.getLevel());
        st.setString(10, data.getStatus());
        st.setString(11, data.getImg());
        st.setString(12, data.getStudentId());
        st.setString(13, data.getEmail());
    }

    @Override
    protected void fillInsertByIdValues(PreparedStatement st, Student data, String id) throws SQLException {

    }

    @Override
    protected String getFindBySearch() {
        return null;
    }

    @Override
    protected void fillFindBySearchValues(PreparedStatement st, String search) throws SQLException {

    }

    @Override
    protected void fillFindByTermValues(PreparedStatement st, String id) {

    }

    @Override
    protected String getFindByTermStatement() {
        return null;
    }

    @Override
    protected void fillFindByEmailValues(PreparedStatement st, String email) throws SQLException{
        st.setString(1, email);
    }

    @Override
    protected String getFindByEmailStatement() {
        return String.format("SELECT " +
                "s.id, " +
                "s.name, " +
                "s.email, " +
                "s.password, " +
                "s.secondName, " +
                "s.birthDate, " +
                "s.field, " +
                "s.faculty, " +
                "s.level, " +
                "s.status, " +
                "s.img " +
                "FROM %s WHERE s.email = ?;", TABLE_NAME + " s");
    }

    @Override
    protected String getFindAllStatement() {
        return null;
    }

    @Override
    protected void fillFindBySearchTypeValues(PreparedStatement st, String search, String type) throws SQLException {
        st.setString(1, search);
        st.setInt(2, type.hashCode());
    }

    @Override
    protected Student convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        if(rs.getString(1) == null)
            return null;
        return new Student(
                rs.getString(1),
                rs.getString(2),
                rs.getString(3),
                rs.getString(4),
                rs.getString(5),
                rs.getString(6),
                rs.getString(7),
                rs.getString(8),
                rs.getString(9),
                rs.getString(10),
                rs.getString(11)
                );
    }

    @Override
    protected String getFindBySearchType() {
        return String.format("SELECT " +
                "id, " +
                "name, " +
                "email, " +
                "password, " +
                "secondName, " +
                "birthDate, " +
                "field, " +
                "faculty, " +
                "level, " +
                "status, " +
                "img " +
                "FROM %s WHERE email = ? AND password = ?;", TABLE_NAME);
    }

    @Override
    protected ArrayList<Student> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        ArrayList<Student> students = new ArrayList<>();
        while (rs.next()) {
            students.add(this.convertResultSetToDomainModel(rs));
        }
        return students;
    }

}

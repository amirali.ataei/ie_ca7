package com.example.CA6.repository;

import com.example.CA6.service.Course;
import org.json.simple.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ExamTimeRepository extends Repository<Course, String> {
    private static final String TABLE_NAME = "ExamTime";
    private static ExamTimeRepository instance;

    public static ExamTimeRepository getInstance() {
        if (instance == null) {
            try {
                instance = new ExamTimeRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in ExamTimeRepository.create query.");
            }
        }
        return instance;
    }

    private ExamTimeRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format("CREATE TABLE IF NOT EXISTS %s (\n" +
                        "  `code` char(225) NOT NULL,\n" +
                        "  `classCode` char(225) NOT NULL,\n" +
                        "  `eStart` char(225) NOT NULL,\n" +
                        "  `eEnd` char(225) NOT NULL,\n" +
                        "  PRIMARY KEY (`code`,`classCode`),\n" +
                        "  CONSTRAINT `classCode1` FOREIGN KEY (`code`, `classCode`) REFERENCES `CourseGroup` (`code`, `classCode`) ON DELETE CASCADE,\n" +
                        "  CONSTRAINT `code1` FOREIGN KEY (`code`, `classCode`) REFERENCES `CourseGroup` (`code`, `classCode`) ON DELETE CASCADE\n" +
                        ");", TABLE_NAME)
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }


    @Override
    protected String getFindByIdStatement() {
        return null;
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, String id) throws SQLException {

    }

    @Override
    protected String getInsertStatement() {
        return String.format("INSERT INTO %s(code, classCode, eStart, eEnd) Select ?, ?, ?, ? Where not exists(Select * from %s Where code = ? AND classCode = ?);", TABLE_NAME, TABLE_NAME);
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, Course data) throws SQLException {
        st.setString(1, data.getCode());
        st.setString(2, data.getClassCode());

        JSONObject examTime = data.getExamTime();
        st.setString(3, (String) examTime.get("start"));
        st.setString(4, (String) examTime.get("end"));
        st.setString(5, data.getCode());
        st.setString(6, data.getClassCode());
    }

    @Override
    protected String getFindAllStatement() {
        return null;
    }

    @Override
    protected void fillFindBySearchTypeValues(PreparedStatement st, String search, String type) throws SQLException {

    }

    @Override
    protected Course convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        return null;
    }

    @Override
    protected String getFindBySearchType() {
        return null;
    }

    @Override
    protected ArrayList<Course> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        return null;
    }

    @Override
    protected void fillInsertByIdValues(PreparedStatement st, Course obj, String id) throws SQLException {

    }

    @Override
    protected String getFindBySearch() {
        return null;
    }

    @Override
    protected void fillFindBySearchValues(PreparedStatement st, String search) throws SQLException {

    }

    @Override
    protected void fillFindByTermValues(PreparedStatement st, String id) {

    }

    @Override
    protected String getFindByTermStatement() {
        return null;
    }

    @Override
    protected void fillFindByEmailValues(PreparedStatement st, String email) throws SQLException {

    }

    @Override
    protected String getFindByEmailStatement() {
        return null;
    }

}

package com.example.CA6.model;

import com.example.CA6.service.Student;
import com.example.CA6.service.Term;

import java.util.ArrayList;

public class Cards extends Datas{
    private ArrayList<Card> cards;

    public Cards() {
        cards = new ArrayList<Card>();
    }

    public Cards(Student student) {
        cards = new ArrayList<Card>();
        Term[] terms = student.getTerms();
        for(int i = 11; i > 0; i--) {
            System.out.println(i);
            if(terms[i].getGrades().size() == 0)
                continue;
            cards.add(new Card(terms[i]));
        }
    }

    public ArrayList<Card> getCards() {
        return cards;
    }
}

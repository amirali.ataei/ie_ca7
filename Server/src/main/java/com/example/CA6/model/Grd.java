package com.example.CA6.model;

import com.example.CA6.service.Grade;

public class Grd {
    private String name;
    private String code;
    private int units;
    private int status;
    private double grade;

    public Grd(Grade grade) {
        this.name = grade.getName();
        this.code = grade.getCode();
        this.units = grade.getUnits();
        this.grade = grade.getGrade();
        if(this.grade >= 10)
            this.status = 1;
        else if(this.grade < 10)
            this.status = 0;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public int getUnits() {
        return units;
    }

    public double getGrade() {
        return grade;
    }

    public int getStatus() {
        return status;
    }
}
